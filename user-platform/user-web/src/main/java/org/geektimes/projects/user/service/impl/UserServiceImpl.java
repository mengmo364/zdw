package org.geektimes.projects.user.service.impl;

import org.geektimes.projects.user.domain.User;
import org.geektimes.projects.user.repository.DatabaseUserRepository;
import org.geektimes.projects.user.service.UserService;
import org.geektimes.projects.user.sql.DBConnectionManager;

import java.sql.SQLException;
import java.util.Collection;

public class UserServiceImpl implements UserService {

    @Override
    public boolean register(User user) throws SQLException {
        DatabaseUserRepository databaseUserRepository = new DatabaseUserRepository(new DBConnectionManager());
        databaseUserRepository.save(user);
        return true;
    }

    @Override
    public boolean deregister(User user) {
        return false;
    }

    @Override
    public boolean update(User user) {
        return false;
    }

    @Override
    public User queryUserById(Long id) {
        return null;
    }

    @Override
    public User queryUserByNameAndPassword(String name, String password) {
        DatabaseUserRepository databaseUserRepository = new DatabaseUserRepository(new DBConnectionManager());
        User user = databaseUserRepository.getByNameAndPassword(name, password);
        return user;
    }
    public Collection<User> getAll() {
        DatabaseUserRepository databaseUserRepository = new DatabaseUserRepository(new DBConnectionManager());
        Collection<User> all = databaseUserRepository.getAll();
        return all;
    }
}
