package org.geektimes.projects.user.web.controller;

import org.geektimes.projects.user.domain.User;
import org.geektimes.projects.user.service.impl.UserServiceImpl;
import org.geektimes.web.mvc.controller.PageController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.Collection;
import java.util.Iterator;

/**
 * 输出 “Hello,World” Controller
 */
@Path("/hello")
public class HelloWorldController implements PageController {

    @GET
    @Path("/world") // /hello/world -> HelloWorldController
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Throwable {
        UserServiceImpl userService = new UserServiceImpl();
        Collection<User> all = userService.getAll();
        if (all != null) {
            Iterator<User> iterator = all.iterator();
            while (iterator.hasNext()) {
                System.out.println("hello world-" + iterator.next().toString());
            }
        }
        return "index.jsp";
    }
}
