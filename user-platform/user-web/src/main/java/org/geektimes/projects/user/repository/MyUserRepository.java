package org.geektimes.projects.user.repository;

import org.geektimes.projects.user.domain.User;
import org.geektimes.projects.user.sql.DBConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

public class MyUserRepository implements UserRepository {

    @Override
    public boolean save(User user) {
        DBConnectionManager dbConnectionManager = new DBConnectionManager();
        Connection connection = dbConnectionManager.getConnection();
        try {
            Statement statement = connection.createStatement();
            String insert = "INSERT INTO users(name,password,email,phoneNumber) VALUES " +
                    "(" + user.getName() + "," + user.getPassword() +
                    "," + user.getEmail() + "," + user.getPhoneNumber() + ")" ;
            System.out.println(statement.executeUpdate(insert));
            System.out.println("保存数据成功--" + user.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("保存数据异常");
        }
        return false;
    }

    @Override
    public boolean deleteById(Long userId) {
        return false;
    }

    @Override
    public boolean update(User user) {
        return false;
    }

    @Override
    public User getById(Long userId) {
        return null;
    }

    @Override
    public User getByNameAndPassword(String userName, String password) {
        return null;
    }

    @Override
    public Collection<User> getAll() {
        return null;
    }
}
