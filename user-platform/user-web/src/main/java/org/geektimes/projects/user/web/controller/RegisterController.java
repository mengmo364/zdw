package org.geektimes.projects.user.web.controller;

import org.geektimes.projects.user.domain.User;
import org.geektimes.projects.user.service.impl.UserServiceImpl;
import org.geektimes.web.mvc.controller.PageController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.Enumeration;

@Path("/register")
public class RegisterController implements PageController {

    @GET
    @POST
    @Path("/user")
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Throwable {
        User user = new User();
        user.setName(request.getParameter("inputName"));
        user.setPassword(request.getParameter("inputPassword"));
        user.setEmail(request.getParameter("inputEmail"));
        user.setPhoneNumber(request.getParameter("inputPhone"));
        System.out.println(user.toString());
        UserServiceImpl userService = new UserServiceImpl();
        userService.register(user);
        user = userService.queryUserByNameAndPassword(user.getName(), user.getPassword());
        System.out.println(user);
        request.getSession().setAttribute("user", user);
        return "registerSuccess.jsp";
    }
}
