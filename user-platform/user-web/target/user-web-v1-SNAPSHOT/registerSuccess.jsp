<head>
<jsp:directive.include
	file="/WEB-INF/jsp/prelude/include-head-meta.jspf" />
<title>Register success</title>
</head>
<body>
	<div class="container-lg" style="color: red">
		<!-- Content here -->
		Register success!
		<table>
			<tr>
				<td>${session.user.id}</td>
				<td>${session.user.name}</td>
				<td>${session.user.email}</td>
				<td>${session.user.phoneNumber}</td>
				<td>${session.user.password}</td>
			</tr>
		</table>
	</div>
</body>